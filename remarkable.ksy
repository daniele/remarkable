meta:
  id: remarkable
  file-extension: rm
  encoding: utf8
  endian: le
doc: "Based on https://gist.github.com/irth/e608dd753cd5233be2fc8b3b50e44c5c"
seq:
  - id: file_header
    type: file_header
  - id: file_version
    type: file_version
    doc: "only '5' was observed in real files by me"
  - id: layers_number
    type: layers_number
  - id: layer
    type: layer
    repeat: expr
    repeat-expr: layers_number.layers_num
types:
  file_header:
    seq:
    - id: magic
      contents: "reMarkable .lines file, "
  file_version:
    seq:
    - id: version_preamble
      contents: "version="
    - id: version
      type: str
      size: 11
      pad-right: 0x20
  layers_number:
    seq:
      - id: layers_num
        type: u4
  layer:
    seq:
      - id: lines_number
        type: u4
      - id: lines
        repeat: expr
        repeat-expr: lines_number
        type: line
  line:
    seq:
      - id: brush_type
        type: u4
        doc: "0x6 eraser. 0xD mechanical pencil, 0x15 calligraphy pen"
      - id: color
        type: u4
        doc: "for calligraphy pen: color 1: gray, color 2: white"
      - id: brush_size
        type: f8
        doc: "This might actually be 4 bytes unknown + 4 bytes brush_size, but when using the eraser tip of the pen also the first 4 bytes are set"
      - id: pad2
        type: str
        size: 4
      - id: points_count
        type: u4
      - id: points
        repeat: expr
        repeat-expr: points_count
        type: point
  point:
    seq:
      - id: x
        type: f4
      - id: y
        type: f4
      - id: speed
        type: f4
      - id: direction
        type: f4
      - id: width
        type: f4
      - id: pressure
        type: f4
